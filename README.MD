# TASK MANAGER

## Developer info
* **Name:** Daria Khrunina
* **Corporate Email:** dkhrunina@t1-consulting.ru
* **Personal Email:** kh.d.m@yandex.ru
## Software
* **OS:** Windows 10
* **JAVA:** OpenJDK 1.8.0_345

## Hardware
* **CPU:** AMD Ryzen 5
* **RAM:** 16GB
* **SSD:** 512GB

## Program run
```shell
java -jar ./tm.jar
```